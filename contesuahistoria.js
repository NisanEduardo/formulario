$( document ).ready(
    function(){
        // Pega os dados do formulário e guarda em JSon
        // getFormData();

        $( 'form input[ type="text" ]' ).each(

            function(){

                $( this ).on( 'input', function(){

                   var elemento = $( this )[ 0 ];
                // console.log( "AQUI" , elemento );
                   mascaraForm( elemento );

                });

            }

        );

    }

);


// Pega os dados do formulário e guarda em JSon
// function getFormData(){

//     var serialize = function (form) {
//         var data = form.serializeArray();
//         var json = {};
//         data.forEach(function (item) {
//             if (!json[item.name]) {
//                 json[item.name] = item.value;
//                 return;
//             }
//             if (!Array.isArray(json[item.name]))
//                 json[item.name] = [json[item.name]];
//             json[item.name].push(item.value);
//         });
//         return json;
//     }

//     var form = $("form");
//     var enviar = $("#enviar");
//     enviar.on("click", function (event) {
//         event.preventDefault();
//         var json = serialize(form);
//         console.log(json);
//     });

// }

function mascara( i ){
   
    var v = i.value;

    // console.log( i );

    // console.log( v );
    
    if(isNaN(v[v.length-1])){ // impede entrar outro caractere que não seja número
       i.value = v.substring(0, v.length-1);
       return;
    }
    
    i.setAttribute("maxlength", "14");
    if (v.length == 3 || v.length == 7) i.value += ".";
    if (v.length == 11) i.value += "-";
 
}

function mascaraForm( elemento ){
    // console.log( elemento.id );
    var valor = elemento.value;

    if( elemento.id == 'cpf' ) {

        if( valor.length == 3 || valor.length == 7 ){

            elemento.value += ".";
    
        } else if( valor.length == 11 ) {
    
            elemento.value += "-";
    
        }

    }

}

// https://pt.stackoverflow.com/questions/290505/m%C3%A1scara-de-entrada-para-cpf-no-formul%C3%A1rio-html-sem-plugin